---
title: About me
subtitle: List of my current interests
comments: false
date: 2018-09-17
---

My name is Ed Barry. I am currently completing my PhD in chemical engineering at the
University of Queensland. My work interests include:

- Process modelling
- Data analysis
- Computational fluid dynamics
- Engineering pedagogy
- Solving problems with computers

In addition to my work activities, I am also really interested in the following
fields:

* Useful and rugged backpacks, messenger bags, and luggage
* Road cycling
* Transport issues and how they can be fixed by designing for active transport
* Black coffee

 I will offer postings from any of my fields of interest.

