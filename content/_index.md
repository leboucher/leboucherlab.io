## Welcome

This is my personal static website, which will include posts about my various
interests.

The site might contain posts about the following topics.  <br></br>
 * Coding and algorithms  <br></br>
 * Data analysis  <br></br>
 * Process modelling  <br></br>
 * Computational Fluid Dynamics  <br></br>
 * Coffee  <br></br>
 * Cycling related content  <br></br>
 * Bags and packs  <br></br>

The previous list is not exhaustive, and there could be posts on other topics
as my interests evolve.
